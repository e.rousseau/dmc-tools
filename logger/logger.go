package logger

import "fmt"

// Verbose define if we should log non error messages
var Verbose = false

// Info prints an unimportant message on the terminal.
func Info(msg string) {
	if Verbose {
		fmt.Println(msg)
	}
}
