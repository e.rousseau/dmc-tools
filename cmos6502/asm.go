// cmos6502 package generates assembly code

package cmos6502

import (
	"fmt"
	"io"
	"strings"
)

// Writer is used to produce 6502 ASM data output
type Writer struct {
	SubWriter io.Writer
	line      string
}

func (w *Writer) Write(p []byte) (n int, err error) {
	if len(w.line) == 0 {
		w.line = "  .db "
	}

	for _, b := range p {
		toAppend := fmt.Sprintf("$%02x,", b)

		if len(w.line)+len(toAppend) > 80 {
			if n, err := w.writeLine(); err != nil {
				return n, err
			}
			w.line = "  .db "
		}
		w.line += toAppend
	}

	return len(p), nil
}

func (w *Writer) writeLine() (n int, err error) {
	return fmt.Fprintln(w.SubWriter, strings.TrimSuffix(w.line, ","))
}
