package audio

import (
	"fmt"
	"io"
)

// DmcWriter is used to write delta modulated samples to a writer
type DmcWriter struct {
	io.Writer
	dmc
	b []byte
}

// DmcPcmWriter is like the DmcWriter but emit in the Writer the resulting
// sample once the delta is applied
type DmcPcmWriter struct {
	io.Writer
	dmc
	b []byte
}

// ResizeWriter make an average of several samples to just one sample. Once
// a sample is computed it's sent into the sub writer. Effectively reduce
// the sample rate without changing the pitch.
type ResizeWriter struct {
	subWriter Writer
	divider   int
	buffer    []int8
	bufPos    int
}

// NewDmcWriter instanciates DmcWriter instances
func NewDmcWriter(writer io.Writer) Writer {
	return &DmcWriter{writer, dmc{}, make([]byte, 1)}
}

// NewDmcPcmWriter instanciates DmcPcmWriter instances
func NewDmcPcmWriter(writer io.Writer) Writer {
	return &DmcPcmWriter{writer, dmc{}, make([]byte, 2)}
}

// NewResizeWriter instanciates ResizeWriter instances
func NewResizeWriter(writer Writer, divider int) Writer {
	return &ResizeWriter{writer, divider, make([]int8, divider), 0}
}

// WriteSample will reduce the sample rate.
func (w *ResizeWriter) WriteSample(v int8) error {
	w.buffer[w.bufPos] = v
	w.bufPos++

	if w.bufPos >= w.divider {
		sample := mean(w.buffer)
		w.subWriter.WriteSample(sample)
		w.bufPos = 0
	}
	return nil
}

// WriteSample write a 8bit sample into it writer. Convert a sample to 8bit
// DMC. See https://en.wikipedia.org/wiki/Delta_modulation for more details.
func (w *DmcWriter) WriteSample(data int8) error {
	if w.step(data) {
		w.b[0] = w.Register
		if _, err := w.Write(w.b); err != nil {
			fmt.Println("Error ", err)
		}
	}
	return nil
}

// WriteSample write a 8bit sample into it writer. Convert a sample to 8bit
// DMC. See https://en.wikipedia.org/wiki/Delta_modulation for more details.
func (w *DmcPcmWriter) WriteSample(data int8) error {
	w.step(data)
	word := int16(w.Tracer) << 9
	w.b[0], w.b[1] = byte(word&0xFF), byte(word>>8)

	if _, err := w.Write(w.b); err != nil {
		fmt.Println("Error ", err)
	}
	return nil
}

func mean(buf []int8) int8 {
	result := 0
	for _, i := range buf {
		result += int(i)
	}
	result /= len(buf)
	return int8(result)
}

// dmc is the unit generating the dmc signal.
type dmc struct {
	Register byte
	Tracer   int8
	Shift    uint
}

func (d *dmc) step(data int8) bool {
	delta := int8(1)
	if d.Tracer >= data {
		delta = -1
	}

	d.Tracer += delta

	dmc := byte(0x80)
	if delta == -1 {
		dmc = 0
	}

	if d.Shift == 0 {
		d.Register = 0
	}

	d.Register = ((d.Register >> 1) | dmc)
	d.Shift++

	reset := d.Shift == 8
	if reset {
		d.Shift = 0
	}

	return reset
}
