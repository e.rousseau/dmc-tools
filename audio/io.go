// audio package convert pcm data to DMC data

package audio

import (
	"fmt"
	"io"
	"os"

	"github.com/youpy/go-wav"
	"gitlab.com/e.rousseau/dmc-tools/logger"
)

// Writer interface of audio data to write
type Writer interface {
	WriteSample(data int8) error
}

// RawWriter is used to generate RAW wav in the format 16bit little
// endian (S16_LE). The sample rate is unknown, it depends on the input format.
type RawWriter struct {
	io.Writer
	b []byte
}

// NewRawWriter instanciates RawWrites instances
func NewRawWriter(file *os.File) Writer {
	return &RawWriter{io.Writer(file), make([]byte, 2)}
}

// WriteSample write a 8bit sample into it writer. The sample is converted
// to signed 16bit little endian (S16_LE)
func (w *RawWriter) WriteSample(v int8) error {
	data := int16(v) << 9
	w.b[0] = byte(data & 0xFF)
	w.b[1] = byte((data >> 8) & 0xFF)

	if _, err := w.Write(w.b); err != nil {
		return err
	}
	return nil
}

// LoadWav loads the file provided as parameter and return it as an
// array of int16 values.
func LoadWav(filename string) (samples []int8, err error) {
	var result []int8

	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	reader := wav.NewReader(file)
	inFormat, err := reader.Format()
	if err != nil {
		return nil, err
	}

	defer file.Close()

	logger.Info(fmt.Sprintf("Channel count : %v", inFormat.NumChannels))
	logger.Info(fmt.Sprintf("Bit depth : %v", inFormat.BitsPerSample))

	bits := inFormat.BitsPerSample

	for {
		samples, err := reader.ReadSamples()

		if err == io.EOF {
			break
		}

		for _, sample := range samples {
			v := reader.IntValue(sample, 0)
			w := int8(0)

			// convert to proper bit format
			switch {
			case bits >= 7:
				w = int8(v >> (bits - 7))
			default:
				w = int8(v << (7 - bits))
			}

			// add to the slice the new data (not sure this its efficient though)
			result = append(result, w)
		}
	}
	return result, nil
}
