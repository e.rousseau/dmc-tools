package main

import (
	"flag"
	"fmt"
	"io"
	"os"

	"gitlab.com/e.rousseau/dmc-tools/audio"
	"gitlab.com/e.rousseau/dmc-tools/cmos6502"
	"gitlab.com/e.rousseau/dmc-tools/logger"
)

var infilePath = flag.String("infile", "", "wav file to read")
var outfilePath = flag.String("outfile", "", "wav file to write")
var verbose = flag.Bool("verbose", false, "show informations")
var format = flag.String("f", "asm", "output format [asm, bin]")
var encoding = flag.String("encoding", "dmc", "encoding format [dmc, raw]")
var resample = flag.Int("resampling", 4, "resampling factor [default = 4]")

func main() {
	flag.Parse()

	// print usage if in and out file aren't defined
	if *infilePath == "" || *outfilePath == "" {
		flag.Usage()
		return
	}

	logger.Verbose = *verbose

	fileOut, err := os.Create(*outfilePath)
	if err != nil {
		fmt.Println(err)
		return
	}

	defer fileOut.Close()

	// transformation stack
	fileWriter := io.Writer(fileOut)

	var formatWriter io.Writer
	switch *format {
	case "bin":
		formatWriter = fileWriter
	case "asm":
		formatWriter = &cmos6502.Writer{SubWriter: fileWriter}
	default:
		fmt.Println("unknown output file format, use asm or bin")
		return
	}

	var writer audio.Writer
	switch *encoding {
	case "dmc":
		writer = audio.NewDmcWriter(formatWriter)
	case "raw":
		writer = audio.NewDmcPcmWriter(formatWriter)
	default:
		fmt.Println("unknown encoding format, use dmc or raw")
		return
	}

	resizeWriter := audio.NewResizeWriter(writer, *resample)

	samples, err := audio.LoadWav(*infilePath)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, s := range samples {
		resizeWriter.WriteSample(s)
	}
}
