# DMC Tools

A command line utility to convert WAV files to DMC format encoded in
6502 assembly.

## To compile

Not sure it compiles outside of my workspace for the moment. Need to review
go lang documentation. Nonetheless, lets give it a try :

    go get gitlab.com/e.rousseau/dmc-tools
    go install gitlab.com/e.rousseau/dmc-tools

## To run

Example :

To convert a wav file to DMC format :

    dmc-tool --infile audio.wav --encoding dmc -f bin --resampling 2 --outfile out.dmc

To convert it to RAW S16_LE format :

    dmc-tool --infile audio.wav --encoding raw -f bin --resampling 2 --outfile out.raw
    
    ; played with aplay under linux
    aplay --rate=10025 --format=S16_LE -c 1 -t raw -D pulse out.raw

    ; or convert it back to wav with SoX
    sox --norm -t s16 -c 1 -r 20050 out.raw -t wav out.wav


To output as 6502 assembly DMC for the NES in a file use the following command :
    
    dmc-tool --infile audio.wav --encoding dmc -f asm --resampling 2 --outfile audio.asm

Enjoy!
